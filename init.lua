--
-- feral
-- By Fudge the Sphinx and Frost Silvermane
--
-- Expects files:
-- feral/init.lua [this code]
-- feral/models/wolf.b3d -- wolf model
-- feral/models/wolfuv.png -- wolf texture
-- feral/models/wieldhand_none.png [empty transparent 16x16 hand sprite, replaces the human one]
--

local storage = minetest.get_mod_storage()

local tf = {}

local function add_command(command, species, mesh, texture, scale, eyeheight, hitbox)
	tf[species] = function(playerName)
		local player = minetest.get_player_by_name(playerName)
		if player == nil then
			minetest.log("error", "[feral] Missing player")
		else
			minetest.log("action", "[feral] Turning "..playerName.." into a "..species)
			player:set_properties({
				visual = "mesh",
				mesh = mesh,
				textures = {texture},
				visual_size = {x=scale, y=scale},
				collisionbox = hitbox
			})
			-- z eye offset just makes your head behave oddly in first person :/
			local offset = {x=0, y=eyeheight, z=0}
			local thirdPersonOffset = {x=0, y=0, z=0}
			player:set_eye_offset(offset, thirdPersonOffset)
			local invetory = player:get_inventory()
			invetory:set_size("hand", 1)
			invetory:set_stack("hand", 1, "feral:paws")
		end
	end
	minetest.register_chatcommand(command, {
		params = "",
		description = "Become a "..species,
		privs = {},
		func = function(playerName, paramStr)
			tf[species](playerName)
		end
	})
	minetest.register_chatcommand("auto"..command, {
		params = "",
		description = "Automatically turn into a "..species.." when you log in",
		privs = {},
		func = function(playerName, paramStr)
			minetest.log("action", "[feral] Registering "..playerName.." as a "..species)
			storage:set_string(playerName, species)
		end
	})
	minetest.register_chatcommand("unauto"..command, {
		params = "",
		description = "Stop turning into a "..species.." when you log in",
		privs = {},
		func = function(playerName, paramStr)
			minetest.log("action", "[feral] Unregistering "..playerName)
			storage:set_string(playerName, "")
		end
	})
end

minetest.register_item("feral:paws", {
	type = "none",
	wield_image = "wieldhand_none.png",
	wield_scale = {x=1,y=1,z=2.5},
	tool_capabilities = {
		full_punch_interval = 0.9,
		max_drop_level = 0,
		groupcaps = {
			crumbly = {times={[2]=3.00, [3]=0.70}, uses=0, maxlevel=1},
			snappy = {times={[3]=0.40}, uses=0, maxlevel=1},
			oddly_breakable_by_hand = {times={[1]=3.50,[2]=2.00,[3]=0.70}, uses=0}
		},
		damage_groups = {fleshy=1}
	}
})

minetest.register_on_joinplayer(function(player)
	local playerName = player:get_player_name()
	local species = storage:get_string(playerName)
	if species ~= nil and species ~= "" then
		minetest.log("action", "[feral] "..playerName.." joined, turning them into a "..species)
		-- short delay to allow the player to spawn in
		minetest.after(0.5, tf[species], playerName)
	end
end)

-- Scale causes hovering characters :/
-- add_command("grayfox", "gray fox", "canid.x", "grayfox.png", 0.7, -8, {-0.4, 0.0, -0.4, 0.4, 0.7, 0.4})
-- add_command("redfox", "red fox", "canid.x", "redfox.png", 0.8, -8, {-0.4, 0.0, -0.4, 0.4, 0.7, 0.4})
-- add_command("wolf", "european wolf", "canid.x", "europeanwolf.png", 1.1, -8, {-0.4, 0.0, -0.4, 0.4, 0.7, 0.4})
-- add_command("puma", "mountain lion", "puma.x", "puma.png", 1.1, -8, {-0.4, 0.0, -0.4, 0.4, 0.7, 0.4})
add_command("wolf", "wolf", "wolf.b3d", "wolfuv.png", 2, -4, {-0.4, 0.0, -0.4, 0.4, 1, 0.4})
