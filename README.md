# Feral

This is a Minetest mod allowing you to be an animal!

The core transformation code was written by Fudge the Sphinx, with Frost adding automatic-transform-on-login and providing the wolf model.

Currently only a wolf is included, although adding new models should be straightforward. If you're comfortable with them being public domain, feel free to submit them for inclusion here!

## Installation

Simply copy this folder to `~/.config/minetest/mods/feral` (or wherever mods are installed on your system).

## Usage

To become a wolf, use the `/wolf` chat command. To automatically transform every time you log in, use `/autowolf` (you can undo this with `/unautowolf`).

## Adding new models

Minetest's engine supports .x and .b3d formats; there's an unofficial B3D exporter for Blender [here][b3d]. The exporter seems to have a hard time with quads, so you'll want to triangulate your model before exporting it. The exporter's default settings seem to work well.

Minetest comes with a Blender file for the default character, you can look at this for the animation ranges. Try looking in /usr/share/minetest/games/minetest_game/mods/player_api/models/.

You should align your model so it's standing on the z=0 plane. Then put it in models/, along with its texture, and add a line to init.lua:

```lua
add_command("<command>", "<species>", "<model>.b3d", "<texture>.png", <scale>, <eye offset>, {-<width>, 0.0, -<width>, <width>, <height>, <width>})
```

You'll need to play around with the eye height and hitbox to make them look right. The eye offset only applies in first person, since in third person you don't want to be looking at the back of your head.

[b3d]: https://github.com/joric/io_scene_b3d/
